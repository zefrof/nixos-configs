{ ... }: {
  services.home-assistant = {
    enable = true;
    extraComponents = [
      # Components required to complete the onboarding
      "analytics"
      "google_translate"
      "met"
      "radio_browser"
      # Recommended for fast zlib compression
      # https://www.home-assistant.io/integrations/isal
      "isal"
      "zwave_js"
    ];
    configWritable = true;
    config = {
      homeassistant = {
        name = "House";
        latitude = "44";
        longitude = "-88";
        elevation = "243";
        unit_system = "imperial";
        time_zone = "America/Menominee";
        temperature_unit = "F";
        currency = "USD";
        country = "US";
      };
      # Includes dependencies for a basic setup
      # https://www.home-assistant.io/integrations/default_config/
      default_config = {};
      http = {
        server_host = "::1";
      	trusted_proxies = [ "::1" ];
      	use_x_forwarded_for = true;
      };
      automation = "!include automations.yaml";
    };
  };

  services.zwave-js = {
    enable = true;
    serialPort = "/dev/serial/by-id/usb-Zooz_800_Z-Wave_Stick_533D004242-if00";
    secretsConfigFile = "/var/zwave/keys.json";
  };

  services.nginx = {
    virtualHosts."ha.dolantube.com" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "http://[::1]:8123";
        proxyWebsockets = true; # needed if you need to use WebSocket
        extraConfig =
          # required when the target is also TLS server with multiple hosts
          "proxy_ssl_server_name on;" +
          # required when the server wants to use HTTP Authentication
          "proxy_pass_header Authorization;";
          # "proxy_buffering off;";
      };
    };
  };
}
