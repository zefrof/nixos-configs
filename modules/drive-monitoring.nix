{ ... }: {

  services.btrfs.autoScrub = {
  	enable = true;
  	interval = "monthly";
  };

  services.smartd = {
  	enable = true;
  	# Run short tests daily and long tests weekly
  	defaults.monitored = "-a -o on -s (S/../.././02|L/../../7/04)";
  };

  services.scrutiny = {
    enable = true;
    collector.enable = true;
    openFirewall = true;
    settings.web.listen.port = 8087;
  };
}
