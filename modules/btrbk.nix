{ ... }: {
  services.btrbk = {
    instances.btrbk.settings = {
      snapshot_preserve_min =
        "2d"; # Keep snapshots on the source for at least 2 days
      snapshot_preserve =
        "7d"; # Keep snapshots on the source for at most 7 days
      target_preserve_min =
        "latest"; # Never delete the latest snapshot from the target
      target_preserve =
        "7d 8w 12m"; # Keep daily snapshots on the target for 7 days, weekly snapshots for 8 weeks, and monthly for 12 months
      volume = {
        "/" = {
          snapshot_dir = ".snapshots"; # Needs to be manually created
          target = "/mnt/backup/snapshots/mofek";
          subvolume = {
            "." =
              { }; # This is not recommended, but is needed until proper subvolumes are setup
          };
        };
        "/mnt/personal" = {
          snapshot_dir =
            "/mnt/personal/.snapshots"; # Needs to be manually created
          target = "/mnt/backup/snapshots/mofek";
          subvolume = { "." = { }; };
        };
      };
    };
  };
}
