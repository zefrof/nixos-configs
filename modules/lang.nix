{ pkgs, ... }: {

  # Select internationalisation properties
  i18n.defaultLocale = "en_US.utf8";

  i18n.inputMethod = {
    enable = true;
    type = "fcitx5";
    fcitx5.addons = with pkgs; [ fcitx5-mozc fcitx5-gtk ];
  };

  # run XDG autostart files
  services.xserver.desktopManager.runXdgAutostartIfNone = true;
}
