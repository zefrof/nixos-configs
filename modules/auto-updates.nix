{ ... }: {

  # Automatic updates, but don't reboot
  system.autoUpgrade = {
    flake = "gitlab:zefrof/nixos-configs";
    enable = true;
    allowReboot = false;
    operation = "boot";
    dates = "weekly";
  };
}
