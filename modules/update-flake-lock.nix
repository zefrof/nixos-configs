{ pkgs, ... }: {
  systemd.services.update-flake-lock = {
    serviceConfig = { Type = "oneshot"; User = "sypha"; };
    path = with pkgs; [ git openssh ];
    # Requires SSH key to be setup with remote
    script = ''
      cd /home/sypha/nixos-configs
      git pull
      /run/current-system/sw/bin/nix flake update --commit-lock-file
      git push origin main
    '';
  };
  systemd.timers.update-flake-lock = {
    wantedBy = [ "timers.target" ];
    partOf = [ "update-flake-lock.service" ];
    timerConfig = {
      OnCalendar = "weekly";
      Unit = "update-flake-lock.service";
      Persistent = true;
    };
  };
}
