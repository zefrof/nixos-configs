{ pkgs, ... }: {

  environment.systemPackages = with pkgs; [
    nautilus
    adwaita-icon-theme
  ];

  # Needed for some functionality
  programs.dconf.enable = true;
  services.gvfs.enable = true;
}
