{ pkgs, ... }: {

  # Wiki setup
  services.mediawiki = {
    enable = true;
    name = "Wiki";
    passwordFile = "/var/mediawiki/passwordFile";
    webserver = "nginx";
    nginx.hostName = "wiki.dolantube.com";
    extraConfig = ''
      # Disable anonymous editing
      $wgGroupPermissions['*']['edit'] = false;
    '';
    database = {
      createLocally = false;
      user = "wiki";
      passwordFile = pkgs.writeText "mediawiki-dbpassword" "pPk=E%8@pW";
      type = "mysql";
      name = "wikidb";
    };
  };

  services.phpfpm.pools.mediawiki.phpOptions = ''
    upload_max_filesize = 10M
    post_max_size = 15M
  '';

  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    initialDatabases = [{ name = "wikidb"; }];
  };

  services.mysqlBackup = {
    enable = true;
    databases = [ "wikidb" ];
  };

  services.nginx = {
    virtualHosts."wiki.dolantube.com" = {
      enableACME = true;
      forceSSL = true;
    };
  };
}
