{ config, pkgs, ... }: {

  # Nextcloud
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud30;
    hostName = "nextcloud.dolantube.com";
    https = true;
    home = "/mnt/storage/nextcloud";
    settings.trusted_domains =  [ "localhost" ];
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "${pkgs.writeText "adminpass" "test123"}";
      adminuser = "admin";
    };
    # extraApps = with pkgs.nextcloud25Packages.apps; {
    #  inherit onlyoffice;
    # };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [{
      name = "nextcloud";
      # ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      ensureDBOwnership = true;
    }];
  };

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = [ "postgresql.service" ];
    after = [ "postgresql.service" ];
  };

  services.nginx = {
    # Required to obtain SSL cert
    virtualHosts."nextcloud.dolantube.com" = {
      enableACME = true;
      forceSSL = true;
    };
  };
}
