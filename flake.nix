{
  description = "My NixOS configs";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-small.url = "github:nixos/nixpkgs/nixos-unstable-small";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    nixos-cosmic = {
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    cosmic-manager = {
      url = "github:HeitorAugustoLN/cosmic-manager";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        home-manager.follows = "home-manager";
      };
    };
  };

  outputs =
    inputs@{ self, nixpkgs, nixpkgs-small, nixpkgs-stable, home-manager, nixos-hardware, nixos-cosmic, cosmic-manager }: {
      nixosConfigurations = {
        midgar = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            {
              nix.settings = {
                substituters = [ "https://cosmic.cachix.org/" ];
                trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=" ];
              };
            }
            nixos-cosmic.nixosModules.default
            ./hosts/midgar/configuration.nix
            nixos-hardware.nixosModules.framework-12th-gen-intel
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.zefrof = {
              	imports = [
              	  ./hosts/midgar/home.nix
              	  cosmic-manager.homeManagerModules.cosmic-manager	
              	];
              };
            }
            ./modules/auto-updates.nix
            ./modules/lang.nix
            ./modules/fonts.nix
            ./modules/gc.nix
            # ./modules/greetd.nix
          ];
        };
        lumar = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            {
              nix.settings = {
                substituters = [ "https://cosmic.cachix.org/" ];
                trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=" ];
              };
            }
            nixos-cosmic.nixosModules.default
            ./hosts/lumar/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.zefrof = {
                imports = [
                  ./hosts/lumar/home.nix
                  cosmic-manager.homeManagerModules.cosmic-manager	
                ];
              };
            }
            ./modules/auto-updates.nix
            ./modules/cosmic.nix
            ./modules/fonts.nix
            ./modules/gc.nix
            # ./modules/greetd.nix
            { _module.args = { inherit inputs; }; }
          ];
        };
        mofek = nixpkgs-stable.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./hosts/mofek/configuration.nix
            ./modules/btrbk.nix
            ./modules/dolantube.nix
            ./modules/drive-monitoring.nix
            ./modules/gc.nix
            ./modules/home-assistant.nix
            ./modules/nginx.nix
            ./modules/update-flake-lock.nix
            ./modules/wiki.nix
          ];
        };
        envy = nixpkgs-stable.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./hosts/envy/configuration.nix
            ./modules/auto-updates.nix
            ./modules/gc.nix
          ];
        };
      };

      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt;
    };
}
