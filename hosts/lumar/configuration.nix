{ config, pkgs, lib, inputs, ... }: {

  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # temporarily override kernel to fix GPU crashes
  boot.kernelPackages = inputs.nixpkgs-small.legacyPackages.${pkgs.system}.linuxPackages; #_latest

  environment.etc."asuka.jpg".source = ./wallpapers/dizzy-michael-sword.jpg;

  networking = {
    hostName = "lumar";
  	networkmanager.enable = true;
  	interfaces.enp5s0 = {
  	  wakeOnLan.enable = true;
  	};
  };

  # Set your time zone.
  time.timeZone = "America/Menominee";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.zefrof = {
    isNormalUser = true;
    description = "Darren";
    extraGroups = [ "networkmanager" "wheel" ];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    btop
    sshfs
    vscode
    # steam
    r2modman
    discord
    firefox-wayland
    pavucontrol
    polkit_gnome
    mpv
    micro
    git
  ];

  # virtualisation.docker.enable = true;

  #Polkit
  security.polkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };
  hardware.steam-hardware.enable = true;
  hardware.graphics.enable32Bit = true;

  # Select internationalisation properties
  i18n.defaultLocale = "en_US.utf8";

  system.stateVersion = "22.11";
}
