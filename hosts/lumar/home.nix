{ pkgs, ... }: {
  home.username = "zefrof";
  home.homeDirectory = "/home/zefrof";
  programs.home-manager.enable = true;
  home.packages = with pkgs; [ mako wl-clipboard kickoff shotman xdg-utils ];
  programs.alacritty = {
    enable = true;
    settings = { window = { opacity = 0.6; }; };
  };
  programs.git = {
    enable = true;
    userName = "zefrof";
    userEmail = "zefrof@protonmail.com";
  };
  wayland.desktopManager.cosmic = {
    # Not managed by nix config:
    # numbered workspaces, hidden dock, wallpaper
    # super+d shortcut for launcher and removing super
    compositor = {
      active_hint = true;
      autotile = true;
      autotile_behavior = "Global";
      focus_follows_cursor = true;
      focus_follows_cursor_delay = 0;
    };
    appearance.toolkit = {
      show_maximize = false;
      show_minimize = false;
    };
  };
  home.stateVersion = "23.11";
}
