{ config, pkgs, ... }: {

  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  fileSystems."/mnt/media" = {
  	label = "storage2";
  	fsType = "btrfs";
  	options = [ "nofail" "compress=zstd" "subvol=@media" ];
  };

  fileSystems."/mnt/personal" = {
  	label = "storage2";
  	fsType = "btrfs";
  	options = [ "nofail" "compress=zstd" "subvol=@personal" ];
  };

  fileSystems."/mnt/backup" = {
    label = "backup";
    fsType = "btrfs";
    options = [ "nofail" ];
  };

  networking.hostName = "mofek"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;
  networking.tempAddresses = "disabled";

  # Set your time zone.
  time.timeZone = "America/Menominee";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.sypha = {
    isNormalUser = true;
    description = "Darren";
    extraGroups = [ "networkmanager" "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDK5c+quzeoRxQrSlk86ovONwUXcAgZZRfZRUqCVWJp7wGnFXXNSjHxPw97jXuU/+38xaOf5nWHupJxRz7qNk/ZvDjmJffIwyO36RfsGFssTzMlE6ibiXd1P7eKtaPf/7HsJuLwlcYFeGyIJqkYl0uzNRMbfVnvKSqqmZSnifY54E3PXPsHj6Em73oz3sAiJawiPy0nGh1ogpwBkbKFdRCOpgeU+p4mUh8D0FAU0GSiVQkki/izXR8VxAwAKOkmUk5uoLABo99VDCpqMaJBJUqdbnyFxzuOyypZqXan1kj3YVPjhw5FjjS6YqwBw9UzbdljyPidtly/ZYLEDvVmzgQf2YPHg8C1UIknFLPkWx/WRCvKcTrOvKk0gTlrPkw7jYIHB59EBfBiNL/Vuehwi2g8TtlDTqBUkNQ7AngY2DniLsA+JfHjYTX4bZAWk5i13CtdizqI3Kdo9VuCMw/LAEAjLfAfry2r6P3iNBEN4/7QntY0Ap9S1Y9Rl2HwFID6jJM= sypha@mofek"
    ];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Automatic updates, reboot on kernal update
  system.autoUpgrade = {
    flake = "gitlab:zefrof/nixos-configs";
    enable = true;
    allowReboot = true;
    dates = "weekly";
    rebootWindow = {
      lower = "04:00";
      upper = "06:00";
    };
  };

  environment.systemPackages = with pkgs; [
    micro
    btop
    git
    docker
    docker-compose
    btrfs-progs
    smartmontools
    tmux
  ];

  # Makes VS code remote work
  programs.nix-ld.enable = true;

  # Enable UPS daemon
  services.apcupsd.enable = true;

  # Docker
  virtualisation.docker = {
    enable = true;
    package = pkgs.docker_25;
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    ports = [ 26354 ];
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 80 443 26354 8081 ];
  networking.firewall.allowedUDPPorts = [ 80 443 26354 8081 ];

  system.stateVersion = "22.11";
}
