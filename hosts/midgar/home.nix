{ pkgs, ... }: {
  home.username = "zefrof";
  home.homeDirectory = "/home/zefrof";
  programs.home-manager.enable = true;
  home.packages = with pkgs; [ mako wl-clipboard kickoff shotman xdg-utils ];
  programs.alacritty = {
    enable = true;
    settings = { window = { opacity = 0.6; }; };
  };
  programs.git = {
    enable = true;
    userName = "zefrof";
    userEmail = "zefrof@protonmail.com";
  };
  /* programs.waybar = { 
    enable = true;
    style = ./waybar-style.css;
  };
  services.mako = {
    enable = true;
    defaultTimeout = 3000;
  };
  wayland.windowManager.sway = {
    enable = true;
    checkConfig = false; # Otherwise rebuilding dies while looking for background pic
    config = rec {
      modifier = "Mod4"; # super key
      terminal = "alacritty";
      menu = "kickoff";
      defaultWorkspace = "workspace number 1";
      bars = [{ command = "waybar"; }];
      gaps = { inner = 5; };
      output = {
        "eDP-1" = {
          mode = "2256x1504@60Hz";
          scale = "1.5";
        };
      };
      input = {
        "*" = { accel_profile = "flat"; };
        "type:touchpad" = {
          click_method = "clickfinger";
        };
      };
      window.titlebar = false;
    };
    extraConfig = ''
      bindsym Mod4+Shift+s         exec shotman -c region
      bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
      bindsym XF86MonBrightnessUp exec brightnessctl set +5%
      bindsym XF86AudioRaiseVolume exec 'pactl set-sink-volume @DEFAULT_SINK@ +5%'
      bindsym XF86AudioLowerVolume exec 'pactl set-sink-volume @DEFAULT_SINK@ -5%'
      bindsym XF86AudioMute exec 'pactl set-sink-mute @DEFAULT_SINK@ toggle'
      bindsym XF86AudioPlay exec playerctl play-pause
      bindsym XF86AudioNext exec playerctl next
      bindsym XF86AudioPrev exec playerctl previous

      bindgesture swipe:right workspace prev
      bindgesture swipe:left workspace next

      output "*" bg /etc/wallpaper.jpg fill
    '';
  }; */
  home.stateVersion = "23.11";
}
