{ config, pkgs, lib, ... }: {

  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  fileSystems = {
    "/home" = {
      options = [ "compress=zstd" "subvol=@home" ];
      fsType = "btrfs";
      device = "/dev/disk/by-uuid/c475e7e0-b69f-4d8b-aa0e-e7f996de9c4d";
    };
  };

  environment.etc."wallpaper.jpg".source = ./wallpapers/ffviir-zoom-tifa.jpg;

  # Cosmic
  services.desktopManager.cosmic.enable = true;
  services.displayManager.cosmic-greeter.enable = true;
  environment.sessionVariables.COSMIC_DATA_CONTROL_ENABLED = 1;

  # hostname
  networking.hostName = "midgar";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone
  time.timeZone = "America/Menominee";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.zefrof = {
    isNormalUser = true;
    description = "Darren";
    # audio & video groups are for brightness and audio adjustemnt using F-keys
    extraGroups = [ "networkmanager" "wheel" "audio" "video" ];
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    # cli
    btop
    micro
    brightnessctl
    playerctl
    pulseaudio # For pactl

    # gui
    vscode
    zed-editor
    steam
    firefox-wayland
    pavucontrol
    mpv
    tdesktop
    anki-bin
    # cura is borked due to new upstream build system
    filezilla
    libreoffice-fresh
    # For spellcheck in Libreoffice
    hunspell
    hunspellDicts.en_US
  ];

  security.polkit.enable = true;
  # services.fprintd.enable - true;

  # Enable CUPS to print documents
  services.printing.enable = true;

  # Allow printer autodiscovery
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };
  hardware.graphics.enable32Bit = true;

  services.flatpak.enable = true;
  xdg.portal = {
    wlr.enable = true;
    configPackages = [ pkgs.gnome-session ];
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  # Enabales TLP and some other power saving settings
  powerManagement.enable = true;

  system.stateVersion = "22.11";
}
